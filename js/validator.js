var TSTriangle = TSTriangle || {};

/**
 * Validator module for triangle app
 * Note: its coded in es5 hence used a module pattern instead of classes
 */
TSTriangle.validator = (function(){

    /**
     * Map which contains all error messages
     */
    var errorMap = {
        emptyMessage: 'Please enter value for side. It can\'t be empty',
        negativeOrZeroMessage: 'Please enter positive value for side. It can\'t be negative or zero',
        invalidTriangleMessage: 'Please enter valid side values'
    }

    /**
     * Method returns the type of triangle
     * @param {array} sides Array containing all 3 side values
     * @returns {string}  Return type of triangle i.e. Equilateral | Isosceles | Scalene
     */
    getTriangleType = function (sides) {
        var side1 = parseFloat(sides[0]);
        var side2 = parseFloat(sides[1]);
        var side3 = parseFloat(sides[2]);

        if (side1 === side2 && side2 === side3) {
            return 'Equilateral';
        }
        else if (side1 === side2
                || side2 === side3
                || side3 === side1) {
            return 'Isosceles';
        }
        else{
            return 'Scalene';
        }
    }

    /**
     * Method to validate all 3 sides of triangle
     * @param {array} sides Array containing all 3 side values
     * @returns {boolean} Returns boolean indicating validation failure / success
     */
    validateInput = function (sides) {
       var detailData = null;

        for(var i=0; i<sides.length ; i++) {
            if(isEmptySides(sides[i])) {
                detailData = {
                    field: "side "+(i+1),
                    error: errorMap.emptyMessage
                }
                break;
            }
            else if(isNegativeOrZero(parseFloat(sides[i]))) {
                detailData = {
                    field: "side "+(i+1),
                    error: errorMap.negativeOrZeroMessage
                }
                break;
            }
        }

        // If there is any error then trigger one event with payload containing error details
        if(detailData != null) {
            var fieldErrorEvent = new CustomEvent("FIELD_ERROR",{detail:detailData});
            document.dispatchEvent(fieldErrorEvent);
            return false;
        }
        else if( ! isTriangleValid(sides)){
            var errorEvent = new CustomEvent("INVALID_TRIANGLE_ERROR",{
                detail:{
                    field:'Invalid Triangle Data',
                    error: errorMap.invalidTriangleMessage
                }
            });
            document.dispatchEvent(errorEvent);
            return false;
        }
        
        return true;
    }

   /**
     * Method to check if passed value is empty string
     * @param {string} side side value of a triangle
     * @returns {boolean} Returns boolean indicating failure / success
     */
    isEmptySides = function (side) {
        return side === "" ? true : false;
    }

     /**
     * Method to check if passed value is negative or zero
     * @param {string} side side value of a triangle
     * @returns {boolean} Returns boolean indicating failure / success
     */
    isNegativeOrZero = function (side) {
        return side <= 0 ? true : false;
    }

     /**
     * Method to check if all 3 sides of triangle are valid
     * @param {array} sides all 3 side values of a triangle
     * @returns {boolean} Returns boolean indicating failure / success
     */
    isTriangleValid = function(sides) {
        // Triangle inequality property : The sum of the lengths of any two sides of a triangle is greater than the length of the third side. 
        //  https://www.mbacrystalball.com/blog/2015/10/16/triangles-properties-types-geometry/

        var side1 = parseFloat(sides[0]);
        var side2 = parseFloat(sides[1]);
        var side3 = parseFloat(sides[2]);

        return (
            side1 + side2 > side3 && 
            side1 + side3 > side2 && 
            side2 + side3 > side1
        );
    }

    return {
        getTriangleType: getTriangleType,
        validateInput: validateInput
    }
})();