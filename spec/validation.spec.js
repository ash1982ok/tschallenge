describe("Triangle Validation", function() {
   
    // NEGATIVE TESTS

    it("should not accept empty side values", function() {
        var testSides = ["","",""];
        var result = TSTriangle.validator.validateInput(testSides);
        expect(result).toBe(false);
    });


    it("should not accept all 0 side values", function() {
        var testSides = [0,0,0];
        var result = TSTriangle.validator.validateInput(testSides);
        expect(result).toBe(false);
    });

    it("should not accept any 0 side values", function() {
        var testSides = [2,3,0];
        var result = TSTriangle.validator.validateInput(testSides);
        expect(result).toBe(false);
    });


    it("is invalid triangle if side doesn't follow 'Triangle inequality property'", function() {
        var testSides = [1,2,1];
        var result = TSTriangle.validator.validateInput(testSides);
        expect(result).toBe(false);
    });


    // POSITIVE TESTS 

    it("should return equilateral triangle", function() {
        var testSides = [1,1,1];
        var result = TSTriangle.validator.getTriangleType(testSides);
        expect(result).toBe("Equilateral");
    });


    it("should return Isosceles triangle", function() {
        var testSides = [4,4,5];
        var result = TSTriangle.validator.getTriangleType(testSides);
        expect(result).toBe("Isosceles");
    });


    it("should return Scalene triangle", function() {
        var testSides = [5,6,7];
        var result = TSTriangle.validator.getTriangleType(testSides);
        expect(result).toBe("Scalene");
    });

});