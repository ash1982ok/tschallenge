/**
 * Namespace for entire application
 */
var TSTriangle = TSTriangle || {};

/**
 * Triangle App: This module will bootstrap the application
 * @param ts Global tradeshift object
 */
TSTriangle.app = (function(ts){
    
    /**
     * Submit handler for triangle form
     */
    var handleSubmit = function() {
            var sides = getSidesDataFromControl();

            if ( TSTriangle.validator.validateInput(sides) ) {
               var triangleType = TSTriangle.validator.getTriangleType(sides);
               if(typeof triangleType !== "undefined") {
                   // Any task can be done here but i am just showing the type in notification
                    ts.ui.Notification.success('Triangle Type is : ' +  triangleType);
               }
            }
    }

    /**
     * Method to fetch side data from input
     */
    var getSidesDataFromControl =  function() {
        var sides = [];
        sides[0] = document.getElementById('triSide1').value;
        sides[1] = document.getElementById('triSide2').value;
        sides[2] = document.getElementById('triSide3').value;
        return sides;
    }

    /**
     * Method to display error on the screen
     * @param {"FIELD_ERROR" | "INVALID_TRIANGLE_ERROR"} event custome event for different error types
     */
    var showErrors = function (event) {
        // Any task can be done here but i am just showing the type in notification
        ts.ui.Notification.error('\n\n' + event.detail.field + "\n\n" + event.detail.error);
    }
    
    return {
        handleSubmit: handleSubmit,
        showErrors: showErrors
    }
})(ts);



// Tradeshift document ready callback
ts.ui.ready(function() {
//Initialization scripts 
document.getElementById("frmTriChallenge").addEventListener('submit', TSTriangle.app.handleSubmit);
document.addEventListener('FIELD_ERROR', TSTriangle.app.showErrors);
document.addEventListener('INVALID_TRIANGLE_ERROR', TSTriangle.app.showErrors);

});