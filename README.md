* Introduction
--------------------------------------------------
Tradeshift triangle challenge developed using vanillajs as the requirement in JD was vanillajs (ES5).
Tried to use as much as ts ui but can't use a lot as this is very small application.

* Installations
--------------------------------------------------

1) npm install

2) npm start

3) localhost:8080 -- for application

4) localhost:8080/SpecRunner.html -- for tests
